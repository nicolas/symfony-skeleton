<?php

namespace App\Command;

use App\Entity\Admin;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:init',
    description: '初始化系统初始数据，包括管理员账户等',
)]
class AppInitCommand extends Command
{
    protected UserPasswordHasherInterface $passwordHasher;

    protected ObjectManager $entityManager;

    public function __construct(
        UserPasswordHasherInterface $passwordHasher,
        ManagerRegistry $entityManager,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->passwordHasher = $passwordHasher;
        $this->entityManager = $entityManager->getManager();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'username',
                'u',
                InputOption::VALUE_OPTIONAL,
                '超级管理员用户名'
            )
            ->addOption(
                'password',
                'p',
                InputOption::VALUE_OPTIONAL,
                '超级管理员密码'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $username = $input->getOption("username") ?? "admin@admin.com";
        $password = $input->getOption("password") ?? "123456";

        $admin = new Admin();
        $admin->setEmail($username);
        $admin->setPassword(
            $this->passwordHasher->hashPassword(
                $admin,
                $password
            )
        );

        $this->entityManager->persist($admin);
        $this->entityManager->flush();

        $io->success('初始化完成.');

        return Command::SUCCESS;
    }
}
