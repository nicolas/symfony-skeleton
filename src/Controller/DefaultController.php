<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route(
        "/index/{id}",
        name: "index",
        requirements: ['id' => '\d+'],
        methods: ['GET']
    )]
    public function index(int $id): Response
    {
        $number = random_int(0, 100);
        return new Response(
            '<html><body>Lucky number: ' . $id . '</body></html>'
        );
    }

    #[Route(
        "/index",
        name: "index2",
        methods: ['GET']
    )]
    public function index2(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $number = random_int(0, 100);
        return new Response(
            '<html><body>Lucky number: ' . $number . '</body></html>'
        );
    }
}
