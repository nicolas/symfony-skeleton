<?php

declare(strict_types=1);

namespace App\Security;

use App\Entity\Admin;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    /**
     * 登录前检测
     *
     * @param UserInterface $user
     * @return void
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof Admin) {
            return;
        }

        if (!$user->isNormal()) {
            throw new CustomUserMessageAccountStatusException('你的账户异常，禁止登录');
        }
    }

    public function checkPostAuth(UserInterface $user)
    {

    }
}
