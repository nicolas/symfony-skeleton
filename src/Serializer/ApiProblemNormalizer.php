<?php

/**
 * [XinFox System] Copyright (c) 2011 - 2021 XINFOX.CN
 */
declare(strict_types=1);

namespace App\Serializer;

use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ApiProblemNormalizer implements NormalizerInterface
{
    #[ArrayShape(['message' => "string", 'code' => "integer", 'class' => "string", 'trace' => "array"])]
    public function normalize($object, string $format = null, array $context = []): array
    {
        return [
            'message' => $object->getMessage(),
            'code' => $object->getStatusCode(),
            'class' => $object->getClass(),
            'trace' => $object->getTrace()
        ];
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof FlattenException;
    }
}